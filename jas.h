#ifndef JAS_H
#define JAS_H

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <math.h>

// CONSTANTS
#ifndef PI
// Borrowed from raylib
#define PI 3.14159265358979323846f
#endif
#define DEG2RAD (PI/180.0f)
#define RAD2DEG (180.0f/PI)

#define internal static
#define global static
#define func_persist static

#define u8 uint8_t
#define u16 uint16_t
#define u32 uint32_t
#define u64 uint64_t
#define s8 int8_t
#define s16 int16_t
#define s32 int32_t
#define s64 int64_t
#define f32 float_t
#define f64 double_t

#define FOR_RANGE(I, S, E) for (int32_t I = S; I < E; I++)

#endif
